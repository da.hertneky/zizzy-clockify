// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDGRD7aAOEhzDn6e7-zxzixtsV4-OLFPdo",
    authDomain: "clockify-clone-21211.firebaseapp.com",
    projectId: "clockify-clone-21211",
    storageBucket: "clockify-clone-21211.appspot.com",
    messagingSenderId: "237550205409",
    appId: "1:237550205409:web:3e237d6dc1224fa295faa0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
