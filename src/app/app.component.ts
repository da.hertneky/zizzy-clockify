import {Component, OnInit} from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Observable} from "rxjs";
import {Task} from "./task/models/task";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[]
})
export class AppComponent implements OnInit{
  sum:any
  obs: Observable<any> | undefined
  title = 'test';
  tasks: Task[] | undefined;

  constructor(private ngf: AngularFirestore) {

  }

  ngOnInit(){
  this.obs = this.ngf.collection("task").valueChanges();
  this.obs.subscribe(tasks=>{
    this.tasks=tasks;
    this.sum = this.tasks?.reduce((prev,curr)=>prev+(curr.end.seconds-curr.start.seconds),0)
  })

  }

}
