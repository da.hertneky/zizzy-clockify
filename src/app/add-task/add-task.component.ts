import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Task} from '../task/models/task';
import {Timestamp} from 'firebase/firestore';
import {AngularFirestore} from '@angular/fire/compat/firestore';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Subscription, timer} from 'rxjs'

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
})
export class AddTaskComponent implements OnInit, OnDestroy {


  running:boolean = false
  task: Task = {
    doc: '',
    description: '',
    start: new Timestamp(0, 0),
    end: new Timestamp(0, 0),
  };
  spent = 0
  sub: any
  taskForm = new FormGroup({
    'description': new FormControl(null,Validators.required),
    'start': new FormControl(new Date()),
    'end': new FormControl(new Date()),
  })

  constructor(private ngf: AngularFirestore) {

  }

  ngOnInit(): void {

  }

  startTimer(): void {
    if (this.sub)
      this.sub.unsubscribe()
    const source = timer(10, 1000);
    this.task.start = Timestamp.fromDate(new Date())
    this.sub = source.subscribe(val => {
      console.log(val, '+');
      this.spent = val;
    });
    this.running = true
  }

  stopTimer(): void {
    this.running=false
    this.task.description = this.taskForm.value.description
    this.task.end = Timestamp.fromDate(new Date(this.task.start.seconds*1000 + this.spent*1000))
    this.sub.unsubscribe()
    let id = this.ngf.createId()
    this.ngf.collection('task').doc(id).set({...this.task, doc: id})
    this.spent=0
  }

  onSubmit(): void {
    if(this.taskForm.invalid)
      return
    console.log(this.taskForm)
    this.task.description = this.taskForm.value.description
    this.task.start = Timestamp.fromDate(this.taskForm.value.start)
    this.task.end = Timestamp.fromDate(this.taskForm.value.end)
    let id = this.ngf.createId()
    this.ngf.collection('task').doc(id).set({...this.task, doc: id})
    this.running=false
  }


  ngOnDestroy() {
    this.sub.unsubscribe()
    this.running=false
  }

}
