import {Component, Input, OnInit} from '@angular/core';
import {Task} from "./models/task"
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {FormControl, Validators} from "@angular/forms";


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  // @ts-ignore
  @Input() task: Task;


  description = new FormControl("",Validators.required)
  start = new FormControl(new Date())
  end = new FormControl(new Date())
  spent = 0

  constructor(private ngf: AngularFirestore) {
  }

  ngOnInit(): void {
    this.calcSpent()
  }

  onDescriptionClick(): void {
    if(this.description.invalid)
      return
    this.ngf.collection('task').doc(this.task.doc).update({description: this.description.value})
      .then(() => console.log("Description successfully updated " + this.description.value))
      .catch((error) => console.error("Error updating description", error))
  }

  onEndClick(): void {
    if (this.end.value.empty)
      return
    this.ngf.collection('task').doc(this.task.doc).update({end: this.end.value})
      .then(() => console.log("End successfully updated" + this.end.value))
      .catch((error) => console.error("Error updating end", error))
  }

  onStartClick(): void {
    if (this.start.value.empty)
      return
    this.ngf.collection('task').doc(this.task.doc).update({start: this.start.value})
      .then(() => console.log("Start successfully updated" + this.start.value))
      .catch((error) => console.error("Error updating start", error))
  }

  onDeleteTask(): void {
    this.ngf.collection('task').doc(this.task.doc).delete()
      .then(() => console.log("Task successfully deleted"))
      .catch((error) => console.error("Error removing task", error))
  }
  calcSpent(){
    this.spent =this.spent=(this.task.end.seconds-this.task.start.seconds)
    console.log(this.spent)
  }
}
