import firebase from "firebase/compat";
import Timestamp = firebase.firestore.Timestamp;

export interface Task {
  doc: string;
  description: string;
  start: Timestamp;
  end: Timestamp;
}
