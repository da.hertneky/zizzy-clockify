import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'timepipe'
})
export class TimepipePipe implements PipeTransform {

  transform(value: any): string {
    if (value == null || typeof (value) != "number")
      return ""
    let sec_num = value;
    let hours = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);
    
    return hours + ':' + minutes + ':' + seconds;
  }
}
