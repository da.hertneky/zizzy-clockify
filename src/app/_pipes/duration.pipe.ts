import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(value: any): string {
    if (value == null || typeof (value) != "number")
      return ""
    let sec_num = value;
    let days = Math.floor(sec_num/(3600*24))
    let hours = Math.floor((sec_num-(days*3600*24)) / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60) - (days*3600*24);

    if(days>0) return days + 'd '+ hours + 'h ' + minutes + 'm ';
    else if(hours>0) return  hours + 'h ' + minutes + 'm ';
    else if(minutes>0)  return minutes + 'm ' + seconds +'s ';
    else return seconds +'s '
  }

}
